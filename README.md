# immo
Sample Carto App to display iris value by colors on top of French Postal Codes

## Installation
- Clone the repository
- Enter the immo folder
- Start the simple proxy server node simpleProxy/server.js (I'll advise nohup or forever to fire & forget the process). This will occupy port 8080
- return to immo folder
`npm run build`
`npm run start`

## parameters
To change the port the proxy server is listening to, pass the env PORT to the script or change head of script

1.`// Listen on a specific host via the HOST environment variable`\
2.`var host = process.env.HOST || '0.0.0.0'`\
3.`// Listen on a specific port via the PORT environment variable`\
4.`var port = process.env.PORT || 8080`

To start the micro app on a different server

`npm run start -- -p 3005``

## MapBox
The Ploygons for Postal Codes are too sharp, the geojson file representing them weight more than 100MB. In case of a further modification of the boundaries, you'll have to rerun the `public/add-real-contours.js`script then import it as the source replacement for the proper tileset
