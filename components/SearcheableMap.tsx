import 'mapbox-gl/dist/mapbox-gl.css'

import * as turf from '@turf/turf'

import React, { useEffect, useRef, useState } from 'react'

import Select from 'react-select'
import VirtualList from 'react-tiny-virtual-list'
import { cpOptions } from '../public/cpList.js'
import mapboxgl from 'mapbox-gl' // eslint-disable-line import/no-webpack-loader-syntax
import styles from '../styles/SearchableMap.module.css'

mapboxgl.accessToken =
  'pk.eyJ1IjoiZnJlZXplMDgwODc4IiwiYSI6ImNraTNhMTlmaDBkeTAyeXN5bHN6cXpoYmEifQ.K3t41EpvXO2PbTe6c1DpuA'

const DefaultItemHeight = 40
let high;

async function fetchMalineContours(cp, high,medium,low) {
  /*
  let irisInfos = await fetch(
    'http://localhost:8080/maline-immobilier.fr/api/get_pub_polygons?code_postal=' +
      cp
  )
  */
  let irisInfos = await fetch(
    '/getcpinfo/api/get_pub_polygons?code_postal=' + cp+'&high='+high+'&medium='+medium+'&low='+low
  )

  const jsonIrisInfo = irisInfos.json()

  return jsonIrisInfo
}

const SOURCE_LAYER_ID = 'contours-codes-postaux-final-dioti2'

type CustomMenuListProps = {
  options: any
  children: any
  maxHeight: number
  getValue: Function
}

class CustomMenuList extends React.Component<CustomMenuListProps> {
  renderItem = (props) => {
    const { children } = this.props
    if (Array.isArray(children)) {
      return (
        <div
          style={props.style}
          className={styles.searchBarElement}
          key={props.index}
        >
          {children[props.index]}
        </div>
      )
    }
    return (
      <div key={props.index} className={styles.searchBarElement}>
        {children.props.children}
      </div>
    )
  }

  render() {

    const { options, children, maxHeight, getValue } = this.props

    const [value] = getValue()
    const initialOffset = options.indexOf(value) * DefaultItemHeight
    const childrenOptions = React.Children.toArray(children)
    const wrapperHeight =
      maxHeight < childrenOptions.length * DefaultItemHeight
        ? maxHeight
        : childrenOptions.length * DefaultItemHeight

    return (
      <span className="react-virtualized-list-wrapper">
        <VirtualList
          width="100%"
          height={wrapperHeight + 6}
          scrollOffset={initialOffset}
          itemCount={childrenOptions.length}
          itemSize={DefaultItemHeight}
          renderItem={this.renderItem}
        />
      </span>
    )
  }
}

type CpSelectProps = {
  selectedCPs: any[]
  setSelectedCPs: Function
}

function CpSelect({ selectedCPs, setSelectedCPs }: CpSelectProps) {
  return (
    <Select
      id="long-value-select"
      instanceId="long-value-select"
      value={selectedCPs}
      isMulti={true}
      /*
      // @ts-ignore */
      options={cpOptions} // !! options is the list of cps !
      captureMenuScroll={false}
      placeholder="Select one or more postal codes"
      // Bring yo new to map instead, easier and no context at this point
      onChange={(selectedValue: any[]) => setSelectedCPs(selectedValue)}
      classNamePrefix="custom-select"
      /*
      // @ts-ignore */
      components={{ MenuList: CustomMenuList }}
    ></Select>
  )
}

function usePrevious(value) {
  const ref = useRef()
  useEffect(() => {
    ref.current = value
  })
  return ref.current
}

export default function SearchableMap() {
  const [high, setHigh] = useState("026600")
  const [medium, setMedium] = useState("fad104")
  const [low, setLow] = useState("cc0202")
  const mapContainer = useRef(null)
  const map = useRef(null)
  const [lng, setLng] = useState(2.2)
  const [lat, setLat] = useState(46.35)
  const [zoom, setZoom] = useState(5)
  const [clickedCPId, setClickedCPId] = useState(null)
  const [selectedCPsInfos, setSelectedCPsInfos] = useState([])
  const prevSelectedCPsInfos = usePrevious(selectedCPsInfos)

  let hoveredCPId = null

  useEffect(() => {
    if (map.current) return // initialize map only once
    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: 'mapbox://styles/freeze080878/ck42kpf5b118p1cqjjt9p8o0v',
      center: [lng, lat],
      zoom: zoom,
    })
  }, [])

  // Effects on changing a selection of postal codes
  useEffect(() => {
    // TODO essayer de faire plus efficace, peut être prendre les selectedCodes avant modification pour faire une soustraction et ne modifier que les features states sr select, qui ont besoin
    const selectedPrevCodes = prevSelectedCPsInfos ? prevSelectedCPsInfos : []
    const selectedCodes = selectedCPsInfos.map((el) => el.value)

    if (!map.current.isStyleLoaded()) return

    const featureState = map.current.getFeatureState({
      source: 'composite',
      sourceLayer: SOURCE_LAYER_ID,
      id: clickedCPId,
    })

    const cpFiltered = map.current.querySourceFeatures(
      'contours-codepostaux',
      {
        sourceLayer: SOURCE_LAYER_ID,
        filter: ['in', ['get', 'codePostal'], ['literal', selectedCodes]],
      },
      () => {
        console.log('queried')
      }
    )

    const cps = map.current.queryRenderedFeatures({
      layers: ['contours-codepostaux-live'],
    })

    cps.forEach((cp) => {
      map.current.setFeatureState(
        {
          source: 'contours-codepostaux',
          sourceLayer: SOURCE_LAYER_ID,
          id: cp.id,
        },
        { selected: false }
      )
    })

    cpFiltered.forEach((cp) => {
      map.current.setFeatureState(
        {
          source: 'contours-codepostaux',
          sourceLayer: SOURCE_LAYER_ID,
          id: cp.id,
        },
        { selected: true }
      )
    })

    // Dans tous les cas on fetch pour le dernier selectionné
    const selectedCodeId = selectedCodes[selectedCodes.length - 1]
    fetchMalineContours(selectedCodeId, high, medium, low)
      .then((irisInfo) => {
        // Do the tansformation and nurture the source
        let currentCpIrisData = {
          type: 'FeatureCollection',
          features: [],
        }

        if (selectedCodes.length == 1) {
          let lastPolygon = []
          for (const [irisCode, irisDetails] of Object.entries(
            irisInfo[selectedCodeId].iris
          )) {
            currentCpIrisData.features.push({
              type: 'Feature',
              properties: {
                color: irisDetails['color'],
              },
              geometry: {
                type: 'Polygon',
                coordinates: irisDetails['geom']['polygons'][0],
              },
            })
            lastPolygon = irisDetails['geom']['polygons'][0]
          }
          const centroid = turf.centroid(turf.polygon(lastPolygon))
          map.current.flyTo({ center: centroid.geometry.coordinates, zoom: 12 })
        }

        map.current.getSource('contours-iris').setData(currentCpIrisData)

        map.current.setFeatureState(
          {
            source: 'contours-codepostaux',
            sourceLayer: SOURCE_LAYER_ID,
            id: selectedCodeId,
          },
          { color: irisInfo[selectedCodeId].color }
        )
      })
      .catch((e) => {
        console.log('Error fetching Iris data', e)
      })
  }, [selectedCPsInfos])

  // Map initializaion behavior
  useEffect(() => {
    if (!map.current) return // wait for map to initialize

    // Sync with Component state
    map.current.on('move', () => {
      setLng(map.current.getCenter().lng.toFixed(4))
      setLat(map.current.getCenter().lat.toFixed(4))
      setZoom(map.current.getZoom().toFixed(2))
    })
    map.current.on('load', () => {
      map.current.resize()

      const layers = map.current.getStyle().layers

      // Find the index of the first symbol layer in the map style
      let firstSymbolId
      for (const layer of layers) {
        //console.log('layer', layer)
        if (layer.type === 'symbol') {
          firstSymbolId = layer.id
          break
        }
      }

      // Add vector source for exact postal codes contours, unique id is code Postal
      map.current.addSource('contours-codepostaux', {
        type: 'vector',
        url: 'mapbox://freeze080878.dkxtlt47',
        promoteId: 'codePostal',
      })

      // We add an empty source for iris details. Data will be provided on every click on a cp. Only one cp iris displayed at a time
      map.current.addSource('contours-iris', {
        type: 'geojson',
        data: {
          type: 'FeatureCollection',
          features: [],
        },
      })

      // Add the fill layer which will paint the whole postal code if two or more are selected, just bedore the symbols
      map.current.addLayer(
        {
          id: 'contours-codepostaux-live',
          type: 'fill',
          source: 'contours-codepostaux',
          'source-layer': SOURCE_LAYER_ID,
          layout: {},
          paint: {
            'fill-color': ['feature-state', 'color'],
            'fill-opacity': ['case', ['==', ['feature-state', 'selected'], true], 0.5, 0.0]
          },
        },
        firstSymbolId
      )

      // Add the line layer which will paint the whole postal code if two or more are selected, just before the symbols
      map.current.addLayer(
        {
          id: 'lines-codepostaux-live',
          type: 'line',
          source: 'contours-codepostaux',
          'source-layer': SOURCE_LAYER_ID,
          layout: {},
          paint: {
            'line-width': ['case', ['boolean', ['feature-state', 'selected'], true],2,1 ],
            'line-color': ['case', ['==', ['get', 'id'], ['feature-state', 'id']],"#448dd6","#FFFFFF" ],
            'line-opacity': ['case', ['==', ['feature-state', 'selected'], true],1
                                    ,['==', ['get', 'id'], ['feature-state', 'id']],1
                            ,0 ],
          },
        },
        firstSymbolId
      )

      // Add the fill layer which will paint the ths iris if one cp is selected, just before the symbols
      map.current.addLayer(
        {
          id: 'contours-iris-live',
          type: 'fill',
          source: 'contours-iris',
          layout: {},
          paint: {
            'fill-color': ['get', 'color'],
            'fill-opacity': 0.5,
            'fill-outline-color': '#FFFFFF',
          },
        },
        firstSymbolId
      )

      map.current.addLayer(
        {
          id: 'contours-iris-live-line',
          type: 'line',
          source: 'contours-iris',
          layout: {},
          paint: {
            'line-width': 2,
            'line-color': '#FFFFFF'
          },
        },
        firstSymbolId
      )
    })

    // SET interaction bahavior
    map.current.on('mousemove', 'contours-codepostaux-live', (e) => {
      if (e.features.length > 0) {
        if (hoveredCPId !== null) {
          map.current.setFeatureState(
            {
              source: 'contours-codepostaux',
              sourceLayer: SOURCE_LAYER_ID,
              id: hoveredCPId,
            },
            { id: null }
          )
        }
        hoveredCPId = e.features[0].id

        if (e.features[0]) {
          map.current.setFeatureState(
            {
              source: 'contours-codepostaux',
              sourceLayer: SOURCE_LAYER_ID,
              id: hoveredCPId,
            },
            { id: e.features[0].properties.id }
          )
        }
      }
    })

    map.current.on('mouseleave', 'contours-codepostaux-live', () => {
      if (hoveredCPId !== null) {
        map.current.setFeatureState(
          {
            source: 'contours-codepostaux',
            sourceLayer: SOURCE_LAYER_ID,
            id: hoveredCPId,
          },
          { id: null }
        )
      }
      hoveredCPId = null
    })

    map.current.on('click', 'contours-codepostaux-live', (e) => {
      if (e.features[0].properties.id) {
        const cpIdClicked = e.features[0].properties.id

        setClickedCPId(cpIdClicked)

        setSelectedCPsInfos((selectedCPsInfos) => {
          // Cp just clicked in current selection ?
          const selectedCPIndex = selectedCPsInfos.findIndex(
            (el) => el.value == cpIdClicked
          )

          // if not add it, if so remobve it from selection
          return selectedCPIndex == -1
            ? [
                ...selectedCPsInfos,
                {
                  value: cpIdClicked,
                  label: cpIdClicked,
                },
              ]
            : selectedCPsInfos.filter((el) => el.value !== cpIdClicked)
        })
      }
    })
  }, [])

  const getHigh = (event)=>{
    setHigh(event.target.value)
  };

  const getMedium = (event)=>{
    setMedium(event.target.value)
  };

  const getLow = (event)=>{
    setLow(event.target.value)
  };

  return (
    <div>
      <div className={styles.searchBar}>
        <CpSelect
          selectedCPs={selectedCPsInfos}
          setSelectedCPs={setSelectedCPsInfos}
        ></CpSelect>
        <div>
        High: <input type="text" value={high} name="high" onChange={getHigh}/>
        Medium: <input type="text" value={medium} name="medium" onChange={getMedium}/>
        Low: <input type="text" value={low} name="low" onChange={getLow}/></div>
      </div>
      <div ref={mapContainer} className={styles.mapContainer} />
    </div>
  )
}
