'use strict'

const fs = require('fs')
const turf = require('turf')

let rawdata = fs.readFileSync('contours-codes-postaux.geojson')

console.log(JSON.parse(rawdata))

let data = JSON.parse(rawdata)

let cpList = []

data.features.forEach((rd) => {
  rd.properties.id = rd.properties.codePostal
  // Add centroid
  //const polygon = turf.polygon(rd.geometry.coordinates)
  //console.log(polygon)
  //console.log(turf.centroid(polygon))
  const cp = rd.properties.codePostal
  cpList.push({ value: cp, label: cp })
})

fs.writeFileSync('contours-codes-postaux-id.geojson', JSON.stringify(data))
fs.writeFileSync(
  'cpList.js',
  'export const cpOptions = ' + JSON.stringify(cpList)
)
