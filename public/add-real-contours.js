'use strict'

const fs = require('fs')
const turf = require('@turf/turf')

const fetch = (...args) =>
  import('node-fetch').then(({ default: fetch }) => fetch(...args))

let rawdata = fs.readFileSync('contours-codes-postaux.geojson')

let data = JSON.parse(rawdata)

let cpList = []

async function fetchMalineContours(cp) {
  let irisInfos = await fetch(
    'https://dev.maline-immobilier.fr/api/get_pub_polygons?code_postal=' + cp
  )
  const jsonIrisInfo = await irisInfos.json()

  if (jsonIrisInfo.length == 0) return await turf.polygon([])

  let precPolygonsItem = null
  let polygonsUnions = null

  console.log('Prcessing CP ' + cp)

  for (const [irisCode, irisDetails] of Object.entries(jsonIrisInfo[cp].iris)) {
    const currentPolygonItem = turf.polygon(irisDetails.geom.polygons[0])

    if (precPolygonsItem !== null) {
      const incrementedPolygonItem =
        polygonsUnions === null ? precPolygonsItem : polygonsUnions
      polygonsUnions = turf.union(incrementedPolygonItem, currentPolygonItem)
    } else polygonsUnions = currentPolygonItem
    precPolygonsItem = currentPolygonItem
  }

  return await polygonsUnions
}

async function mainMalineRequestChain(data) {
  for (const rd of data.features) {
    rd.properties.id = rd.properties.codePostal

    if (parseInt(rd.properties.id)) {
      const malineContours = await fetchMalineContours(rd.properties.codePostal)

      rd.geometry = malineContours.geometry
    }
  }

  return await data
}

mainMalineRequestChain(data)
  .then((completedData) => {
    console.log('Processing Done')
    fs.writeFileSync(
      'contours-codes-postaux-final.geojson',
      JSON.stringify(completedData)
    )
  })
  .catch((e) => console.warn('Probleme sur recupértion', e))
