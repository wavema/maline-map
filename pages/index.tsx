import React, { ReactElement } from 'react'

import Head from 'next/head'
import SearcheableMap from '../components/SearcheableMap'
import styles from '../styles/Home.module.css'

export default function Home() {
  return <SearcheableMap></SearcheableMap>
}

Home.getLayout = function getLayout(page: ReactElement) {
  return (
    <div className={styles.searchableMapContainer}>
      <Head>
        <title>Maline Immobilier Carto App</title>
        <meta name="description" content="Layered Carto App for Ad creation" />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon-16x16.png"
        />
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
        <meta name="msapplication-TileColor" content="#da532c" />
        <meta name="theme-color" content="#ffffff"></meta>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Barlow+Condensed:ital,wght@0,100;0,400;1,100;1,200&family=Quicksand:wght@300&family=Epilogue:wght@500&family=Ubuntu+Condensed&display=swap"
          rel="stylesheet"
        />
      </Head>

      <div className={styles.searchableMapContainer}>{page}</div>
    </div>
  )
}
