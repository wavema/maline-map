import type { ReactElement, ReactNode } from 'react'

import type { AppProps } from 'next/app'
import type { NextPage } from 'next'

/**
 * Adding necessary types for nex.js layout managament
 */
type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode
}

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout
}

function MyApp({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ?? ((page) => page)

  return getLayout(<Component {...pageProps} />)
}

export default MyApp